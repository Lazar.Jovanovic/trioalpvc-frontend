import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http : HttpClient) { }

  authenticateUser(loginInfo: any){
    return this.http.post(`${environment.baseUrl}/login`, loginInfo).toPromise()
  }

  verifyRefresh(refreshToken: any){
    return this.http.post(`${environment.baseUrl}/token`, refreshToken).toPromise()
  }

  deleteToken(refreshToken:any){
    return this.http.request('DELETE', `${environment.baseUrl}/logout`, {
      body: { refreshToken: refreshToken }
  }).toPromise();
  }


  tokenExpired(token: string) {
    if(!token){
      return true;
    }
    const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
    return (Math.floor((new Date).getTime() / 1000)) >= expiry;
  }



}

