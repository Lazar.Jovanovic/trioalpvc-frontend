import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  constructor(private http: HttpClient) { }

  viewImages(limit, page){
    return this.http.get(`${environment.baseUrl}/gallery?limit=${limit}&page=${page}`).toPromise()
  }
  newImage(imageData){
    return this.http.post(`${environment.baseUrl}/gallery`, imageData).toPromise()
  }
  updateImage(imageData, id){
    return this.http.put(`${environment.baseUrl}/gallery/${id}`, imageData).toPromise()
  }
  deleteImage(id){
    return this.http.delete(`${environment.baseUrl}/gallery/${id}`).toPromise()
  }

}
