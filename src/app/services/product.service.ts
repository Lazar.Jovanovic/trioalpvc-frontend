import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }

  findProducts(limit, page){
    return this.http.get(`${environment.baseUrl}/products?limit=${limit}&page=${page}`).toPromise()
  }
  deleteProduct(id){
    return this.http.delete(`${environment.baseUrl}/products/${id}`).toPromise()
  }
  addProduct(data){
    console.log(this.http.post(`${environment.baseUrl}/product`, data))
    return this.http.post(`${environment.baseUrl}/product`, data).toPromise()
  }
  findProduct(id){
    return this.http.get(`${environment.baseUrl}/products/${id}`).toPromise()
  }
  updateProduct(id, data){
    return this.http.put(`${environment.baseUrl}/products/${id}`, data).toPromise()
  }
  deleteImage(id, url){
    return this.http.put(`${environment.baseUrl}/products/${id}/image`, url).toPromise()
  }
  undeleteProduct(id){
    return this.http.delete(`${environment.baseUrl}/products/${id}/undelete`).toPromise()
  }
  getDeletedProducts(limit, page){
    return this.http.get(`${environment.baseUrl}/deleted/products?limit=${limit}&page=${page}`).toPromise()
  }
}

