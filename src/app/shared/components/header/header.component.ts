import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService} from '../../../services/auth.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  refreshToken = localStorage.getItem('refreshToken')
  constructor(private authService:AuthService, private router: Router) { }

  ngOnInit(): void {
  }
  binNavigate(){
    this.router.navigateByUrl('/bin')
  }
  productsNavigate(){
    this.router.navigateByUrl('/products')
  }

  galleryNavigate(){
    this.router.navigateByUrl('/gallery')
  }

  logout = async()=>{
    try {
      localStorage.removeItem("refreshToken");
      localStorage.removeItem("token");
      this.router.navigateByUrl('/login');
      await this.authService.deleteToken({refreshToken:this.refreshToken})
    } catch (error) {
      console.log(error)
    }
  }

}
