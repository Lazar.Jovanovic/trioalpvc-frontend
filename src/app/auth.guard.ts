import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(): boolean {
    try {
      if (!this.authService.tokenExpired(localStorage.getItem('token'))) {
        return true;
      }
      let refreshToken = localStorage.getItem('refreshToken')
      this.authService.deleteToken({refreshToken:refreshToken});
      localStorage.removeItem("refreshToken");
      localStorage.removeItem("token");

      this.router.navigateByUrl('/login');
      return false;
    } catch (error) {
      console.log(error)
    }
  }


}
