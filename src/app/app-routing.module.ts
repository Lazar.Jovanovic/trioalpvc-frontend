import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from './auth.guard'
const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    {path: 'login', loadChildren: () => import('src/app/modules/login/login.module').then(m => m.LoginModule)},
    {path: 'products',loadChildren: ()=>import('src/app/modules/products/products.module').then(m=>m.ProductsModule) ,canActivate: [AuthGuard]},
    {path: 'gallery',loadChildren: ()=>import('src/app/modules/gallery/gallery.module').then(m=>m.GalleryModule) ,canActivate: [AuthGuard]},
    {path: 'bin',loadChildren: ()=>import('src/app/modules/bin/bin.module').then(m=>m.BinModule) ,canActivate: [AuthGuard]},
    { path: '**', redirectTo: 'login' }
  ]), RouterModule.forChild([
    { path: '', redirectTo: 'login', pathMatch: 'full'}

  ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
