import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import { environment } from 'src/environments/environment';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Product } from 'src/app/model/product';

@Component({
  selector: 'app-one-product',
  templateUrl: './one-product.component.html',
  styleUrls: ['./one-product.component.css']
})

export class OneProductComponent implements OnInit {
  formData: any = new FormData();
  form: FormGroup
  id
  product: Product
  file
  baseUrl = environment.shortUrl
  test
  constructor(private formBuilder: FormBuilder, private router2: ActivatedRoute, private router: Router, private productService: ProductService) { }

  ngOnInit(): void {
    this.findProduct()
  }
  ngAfterViewChecked() {
    let textareas = document.getElementsByTagName('textarea')
    for (let i = 0; i < textareas.length; i++) {
      const textarea = textareas[i];
      this.adjustTextarea(textarea)
    }
    let publishedValue = (<HTMLInputElement>document.getElementById('publishedSelect'))
    this.checkStatusOnViewChecked(publishedValue)
  }
  findProduct = async () => {
    try {
      let id = this.router2.snapshot.paramMap.get('id')
      let response = await this.productService.findProduct(id)
      this.product = response['product']
    } catch (error) {
      this.router.navigateByUrl('/products')
      console.log(error)
    }
  }
  deleteImage(imageUrl, index) {
    let div = document.getElementById('oneImageDiv' + index)
    div.remove()
    let id = this.router2.snapshot.paramMap.get('id')
    console.log(imageUrl)
    let url = { url: imageUrl }
    this.productService.deleteImage(id, url)
  }
  onFileSelect(event) {
    this.file = (event.target as HTMLInputElement).files;
    console.log(this.file)
    this.form.patchValue({
      Images: this.file
    });
    this.form.get('Images').updateValueAndValidity()
  }
  updateProduct = async () => {
    try {
      let id = this.router2.snapshot.paramMap.get('id')
      let title = (<HTMLTextAreaElement>document.getElementById('productTitle')).value
      let type = (<HTMLInputElement>document.getElementById('productTypeArea')).value
      let brand = (<HTMLTextAreaElement>document.getElementById('productBrandArea')).value
      let code = (<HTMLTextAreaElement>document.getElementById('productCodeArea')).value
      let desc = (<HTMLTextAreaElement>document.getElementById('descriptionText')).value
      let published = (<HTMLInputElement>document.getElementById('publishedSelect')).value

      let infoAndTable = document.getElementById('infoAndTable')
      let info = new Array()
      infoAndTable.childNodes.forEach(element => {
        if (element.nodeName === 'DIV') {
          let mainDiv = element
          let contentDiv = mainDiv.firstChild
          contentDiv.childNodes.forEach(element => {
            if (element.nodeName === "P" || element.nodeName === "H2" || element.nodeName === "DIV") {
              if (element.nodeName === "P") {
                let p = element
                let textValue = (<HTMLTextAreaElement>p.firstChild).value
                info.push({ type: 'text', text: textValue })
              }
              if (element.nodeName === "H2") {
                let h2 = element
                let textValue = (<HTMLTextAreaElement>h2.firstChild).value
                info.push({ type: 'subtitle', text: textValue })
              }
              if (element.nodeName === "DIV") {
                let divElement = element
                let table = divElement.firstChild
                let trHead = table.childNodes[0]
                let headArray = new Array()
                let dataArray = new Array()
                trHead.childNodes.forEach(element => {
                  if (element.nodeName === "TH") {
                    let th = element
                    let headValue = (<HTMLTextAreaElement>th.firstChild).value
                    headArray.push(headValue)
                  }
                })
                for (let i = 1; i < table.childNodes.length; i++) {
                  if (table.childNodes[i].nodeName === "TR") {
                    let trData = table.childNodes[i]
                    let dataArray2 = new Array()
                    trData.childNodes.forEach(td => {
                      if (td.nodeName === "TD") {
                        let dataValue = (<HTMLTextAreaElement>td.firstChild).value
                        dataArray2.push(dataValue)
                      }
                    })
                    dataArray.push(dataArray2)
                  }
                }
                info.push({ tableHeaders: headArray, tableData: dataArray })
              }
            }
          })
        }
      });
      this.formData.append("title", title)
      this.formData.append("type", type)
      this.formData.append("brand", brand)
      this.formData.append("code", code)
      this.formData.append("info[]", JSON.stringify(info))
      this.formData.append("desc", desc)
      this.formData.append("published", published)
      if (this.file) {
        for (let i = 0; i < this.file.length; i++) {
          this.formData.append("images[]", this.file[i])
        }
      }
      await this.productService.updateProduct(id, this.formData)
      window.location.reload()
    } catch (error) {
      console.log({ error })
    }
  }
  addSubtitle() {
    let infoAndTable = document.getElementById('infoAndTable')

    let mainDiv = document.createElement('div')
    let contentDiv = document.createElement('div')
    contentDiv.id = 'textOrTableDiv'
    contentDiv.style.position = 'relative'
    contentDiv.style.paddingLeft = '60px'
    contentDiv.style.textAlign = 'center'
    contentDiv.style.width = '90%'
    contentDiv.style.height = '100%'
    contentDiv.style.float = 'left'

    let h2 = document.createElement('h2')
    let textarea = document.createElement('textarea')
    textarea.className = 'informationSubtitle'
    textarea.style.fontFamily = "Times New Roman, Times, serif"
    textarea.style.resize = 'none'
    textarea.style.textAlign = 'center'
    textarea.style.fontSize = '24px'
    textarea.style.width = '50%'
    textarea.style.border = 'none'
    textarea.textContent = 'Add Subtitle'

    let deleteButton = document.createElement('button')
    deleteButton.textContent = 'X'
    deleteButton.style.position = 'absolute'
    deleteButton.style.top = '0px'
    deleteButton.style.right = '-10px'
    deleteButton.style.borderRadius = '10px'
    deleteButton.style.backgroundColor = 'white'
    deleteButton.style.borderWidth = 'thin'
    deleteButton.style.display = 'block'
    deleteButton.addEventListener('click', function () {
      mainDiv.remove()
    })

    h2.appendChild(textarea)
    contentDiv.appendChild(h2)
    contentDiv.appendChild(deleteButton)
    mainDiv.appendChild(contentDiv)
    infoAndTable.appendChild(mainDiv)
  }
  addText() {
    let infoAndTable = document.getElementById('infoAndTable')

    let mainDiv = document.createElement('div')
    let contentDiv = document.createElement('div')
    contentDiv.id = 'textOrTableDiv'
    contentDiv.style.position = 'relative'
    contentDiv.style.paddingLeft = '60px'
    contentDiv.style.textAlign = 'center'
    contentDiv.style.width = '90%'
    contentDiv.style.height = '100%'
    contentDiv.style.float = 'left'


    let p = document.createElement('p')
    let textarea = document.createElement('textarea')
    textarea.className = 'informationText'
    textarea.style.fontFamily = "Times New Roman, Times, serif"
    textarea.style.paddingLeft = '4px'
    textarea.style.paddingRight = '4px'
    textarea.style.resize = 'none'
    textarea.style.textAlign = 'center'
    textarea.style.fontSize = '20px'
    textarea.style.width = '90%'
    textarea.style.border = 'none'
    textarea.textContent = 'Add Text'

    let deleteButton = document.createElement('button')
    deleteButton.textContent = 'X'
    deleteButton.style.position = 'absolute'
    deleteButton.style.top = '0px'
    deleteButton.style.right = '-10px'
    deleteButton.style.borderRadius = '10px'
    deleteButton.style.backgroundColor = 'white'
    deleteButton.style.borderWidth = 'thin'
    deleteButton.style.display = 'block'
    deleteButton.addEventListener('click', function () {
      mainDiv.remove()
    })

    p.appendChild(textarea)
    contentDiv.appendChild(p)
    contentDiv.appendChild(deleteButton)
    mainDiv.appendChild(contentDiv)
    infoAndTable.appendChild(mainDiv)
  }
  addTable() {
    let infoAndTable = document.getElementById('infoAndTable')

    let mainDiv = document.createElement('div')

    let contentDiv = document.createElement('div')
    contentDiv.id = 'textOrTableDiv'
    contentDiv.style.position = 'relative'
    contentDiv.style.paddingLeft = '60px'
    contentDiv.style.textAlign = 'center'
    contentDiv.style.width = '90%'
    contentDiv.style.height = '100%'
    contentDiv.style.float = 'left'

    let oneTableDiv = document.createElement('div')
    oneTableDiv.style.textAlign = 'center'
    oneTableDiv.style.marginTop = '20px'
    oneTableDiv.style.marginBottom = '3%'
    oneTableDiv.style.marginLeft = '95px'
    oneTableDiv.style.width = '100%'

    let table = document.createElement('table')
    table.style.position = 'relative'
    table.style.borderCollapse = 'collapse'
    table.style.borderRadius = '15px'
    table.style.borderStyle = 'hidden'
    table.style.boxShadow = '0 0 0 1px #666'
    table.style.width = '80%'
    let tr1 = document.createElement('tr')
    let tr2 = document.createElement('tr')

    let th1 = document.createElement('th')
    th1.style.padding = '10px'
    th1.style.border = '1px solid black'
    th1.style.backgroundColor = 'rgb(235,235,235)'
    th1.style.borderRadius = '15px 0px 0px 0px'
    let th2 = document.createElement('th')
    th2.style.padding = '10px'
    th2.style.border = '1px solid black'
    th2.style.backgroundColor = 'rgb(235,235,235)'
    let th3 = document.createElement('th')
    th3.style.padding = '10px'
    th3.style.border = '1px solid black'
    th3.style.backgroundColor = 'rgb(235,235,235)'
    th3.style.borderRadius = '0px 15px 0px 0px'

    let textareaH1 = document.createElement('textarea')
    textareaH1.textContent = 'add header'
    textareaH1.addEventListener('keyup', function () {
      this.style.height = '1px'
      this.style.height = (this.scrollHeight) + "px";
    })
    textareaH1.style.resize = 'none'
    textareaH1.style.textAlign = 'center'
    textareaH1.style.fontSize = '17px'
    textareaH1.style.border = 'none'
    textareaH1.style.backgroundColor = 'rgb(235,235,235)'
    textareaH1.style.width = '100%'
    textareaH1.style.height = '21px'
    textareaH1.style.fontFamily = "Times New Roman, Times, serif"
    let textareaH2 = document.createElement('textarea')
    textareaH2.textContent = 'add header'
    textareaH2.addEventListener('keyup', function () {
      this.style.height = '1px'
      this.style.height = (this.scrollHeight) + "px";
    })
    textareaH2.style.resize = 'none'
    textareaH2.style.textAlign = 'center'
    textareaH2.style.fontSize = '17px'
    textareaH2.style.border = 'none'
    textareaH2.style.backgroundColor = 'rgb(235,235,235)'
    textareaH2.style.width = '100%'
    textareaH2.style.height = '21px'
    textareaH2.style.fontFamily = "Times New Roman, Times, serif"
    let textareaH3 = document.createElement('textarea')
    textareaH3.textContent = 'add header'
    textareaH3.addEventListener('keyup', function () {
      this.style.height = '1px'
      this.style.height = (this.scrollHeight) + "px";
    })
    textareaH3.style.resize = 'none'
    textareaH3.style.textAlign = 'center'
    textareaH3.style.fontSize = '17px'
    textareaH3.style.border = 'none'
    textareaH3.style.backgroundColor = 'rgb(235,235,235)'
    textareaH3.style.width = '100%'
    textareaH3.style.height = '21px'
    textareaH3.style.fontFamily = "Times New Roman, Times, serif"

    th1.appendChild(textareaH1)
    th2.appendChild(textareaH2)
    th3.appendChild(textareaH3)

    let td1 = document.createElement('td')
    td1.style.padding = '10px'
    td1.style.border = '1px solid black'
    let td2 = document.createElement('td')
    td2.style.padding = '10px'
    td2.style.border = '1px solid black'

    let td3 = document.createElement('td')
    td3.style.padding = '10px'
    td3.style.border = '1px solid black'

    let textareaD1 = document.createElement('textarea')
    textareaD1.textContent = 'add data'
    textareaD1.addEventListener('keyup', function () {
      this.style.height = '1px'
      this.style.height = (this.scrollHeight) + "px";
    })
    textareaD1.style.resize = 'none'
    textareaD1.style.textAlign = 'center'
    textareaD1.style.fontSize = '17px'
    textareaD1.style.border = 'none'
    textareaD1.style.width = '100%'
    textareaD1.style.height = '21px'
    textareaD1.style.fontFamily = 'Times New Roman, Times, serif'

    let textareaD2 = document.createElement('textarea')
    textareaD2.textContent = 'add data'
    textareaD2.addEventListener('keyup', function () {
      this.style.height = '1px'
      this.style.height = (this.scrollHeight) + "px";
    })
    textareaD2.style.resize = 'none'
    textareaD2.style.textAlign = 'center'
    textareaD2.style.fontSize = '17px'
    textareaD2.style.border = 'none'
    textareaD2.style.width = '100%'
    textareaD2.style.height = '21px'
    textareaD2.style.fontFamily = 'Times New Roman, Times, serif'

    let textareaD3 = document.createElement('textarea')
    textareaD3.textContent = 'add data'
    textareaD3.addEventListener('keyup', function () {
      this.style.height = '1px'
      this.style.height = (this.scrollHeight) + "px";
    })
    textareaD3.style.resize = 'none'
    textareaD3.style.textAlign = 'center'
    textareaD3.style.fontSize = '17px'
    textareaD3.style.border = 'none'
    textareaD3.style.width = '100%'
    textareaD3.style.height = '21px'
    textareaD3.style.fontFamily = 'Times New Roman, Times, serif'

    let deleteButton = document.createElement('button')
    deleteButton.textContent = 'X'
    deleteButton.style.position = 'absolute'
    deleteButton.style.top = '0px'
    deleteButton.style.right = '-10px'
    deleteButton.style.borderRadius = '10px'
    deleteButton.style.backgroundColor = 'white'
    deleteButton.style.borderWidth = 'thin'
    deleteButton.style.display = 'block'
    deleteButton.addEventListener('click', function () {
      mainDiv.remove()
    })

    let addColumn = document.createElement('button')
    addColumn.textContent = '+'
    addColumn.style.position = 'absolute'
    addColumn.style.top = '0px'
    addColumn.style.right = '-10px'
    addColumn.style.borderRadius = '10px'
    addColumn.style.backgroundColor = 'white'
    addColumn.style.borderWidth = 'thin'
    addColumn.addEventListener('click', function () {
      table.childNodes.forEach((tr, index) => {
        if (tr.nodeName === 'TR' && index === 0) {
          let trH = table.childNodes[0]
          let th = document.createElement('th')
          th.style.padding = '10px'
          th.style.border = '1px solid black'
          th.style.backgroundColor = 'rgb(235,235,235)'
          th.style.borderRadius = '0px 15px 0px 0px'

          let textarea = document.createElement('textarea')
          textarea.textContent = 'add header'
          textarea.addEventListener('keyup', function () {
            this.style.height = '1px'
            this.style.height = (this.scrollHeight) + "px";
          })
          textarea.style.resize = 'none'
          textarea.style.textAlign = 'center'
          textarea.style.fontSize = '17px'
          textarea.style.border = 'none'
          textarea.style.backgroundColor = 'rgb(235,235,235)'
          textarea.style.width = '100%'
          textarea.style.height = '21px'
          textarea.style.fontFamily = "Times New Roman, Times, serif"

          th.appendChild(textarea)
          trH.appendChild(th)
        }
        if (tr.nodeName === 'TR' && index !== 0) {
          let td = document.createElement('td')
          td.style.padding = '10px'
          td.style.border = '1px solid black'

          let textarea = document.createElement('textarea')
          textarea.addEventListener('keyup', function () {
            this.style.height = '1px'
            this.style.height = (this.scrollHeight) + "px";
          })
          textarea.textContent = 'add data'
          textarea.style.resize = 'none'
          textarea.style.textAlign = 'center'
          textarea.style.fontSize = '17px'
          textarea.style.border = 'none'
          textarea.style.width = '100%'
          textarea.style.height = '21px'
          textarea.style.fontFamily = "Times New Roman, Times, serif"

          td.appendChild(textarea)
          tr.appendChild(td)
        }
      });
    })

    let removeColumn = document.createElement('button')
    removeColumn.textContent = '-'
    removeColumn.style.position = 'absolute'
    removeColumn.style.top = '30px'
    removeColumn.style.width = '21px'
    removeColumn.style.right = '-10px'
    removeColumn.style.borderRadius = '10px'
    removeColumn.style.backgroundColor = 'white'
    removeColumn.style.borderWidth = 'thin'
    removeColumn.addEventListener('click', function () {
      let row = table.rows
      for (let i = 0; i < row.length; i++) {
        let last = row[i].cells.length - 1
        row[i].deleteCell(last)
      }
    })

    let addRow = document.createElement('button')
    addRow.textContent = '+'
    addRow.style.position = 'absolute'
    addRow.style.bottom = '0px'
    addRow.style.left = '-10px'
    addRow.style.width = '21px'
    addRow.style.borderRadius = '10px'
    addRow.style.backgroundColor = 'white'
    addRow.style.borderWidth = 'thin'
    addRow.addEventListener('click', function () {
      let newRow = table.insertRow(table.rows.length)
      let row = table.rows
      for (let i = 0; i < row[0].cells.length; i++) {
        let cell = newRow.insertCell()
        let textarea = document.createElement('textarea')
        textarea.textContent = 'add data'
        textarea.style.resize = 'none'
        textarea.style.textAlign = 'center'
        textarea.style.fontSize = '17px'
        textarea.style.border = 'none'
        textarea.style.width = '100%'
        textarea.style.height = '21px'
        textarea.style.fontFamily = "Times New Roman, Times, serif"
        textarea.addEventListener('keyup', function () {
          this.style.height = '1px'
          this.style.height = (this.scrollHeight) + "px";
        })
        cell.style.padding = '10px'
        cell.style.border = '1px solid black'
        cell.appendChild(textarea)
      }
    })

    let removeRow = document.createElement('button')
    removeRow.textContent = '-'
    removeRow.style.position = 'absolute'
    removeRow.style.bottom= '30px'
    removeRow.style.left = '-10px'
    removeRow.style.width = '21px'
    removeRow.style.borderRadius = '10px'
    removeRow.style.backgroundColor = 'white'
    removeRow.style.borderWidth = 'thin'
    removeRow.addEventListener('click', function () {
      table.deleteRow(table.rows.length - 1)
    })

    td1.appendChild(textareaD1)
    td2.appendChild(textareaD2)
    td3.appendChild(textareaD3)

    tr1.appendChild(th1)
    tr1.appendChild(th2)
    tr1.appendChild(th3)

    tr2.appendChild(td1)
    tr2.appendChild(td2)
    tr2.appendChild(td3)

    table.appendChild(tr1)
    table.appendChild(tr2)
    table.appendChild(addColumn)
    table.appendChild(removeColumn)
    table.appendChild(addRow)
    table.appendChild(removeRow)

    oneTableDiv.appendChild(table)
    contentDiv.appendChild(oneTableDiv)
    contentDiv.appendChild(deleteButton)
    mainDiv.appendChild(contentDiv)
    infoAndTable.appendChild(mainDiv)
  }

  deleteTextOrTable(i) {
    let textOrTableDiv = document.getElementById('textOrTableDiv' + i)
    let parentDiv = textOrTableDiv.parentElement
    parentDiv.remove()
  }
  addColumn(element) {
    let table = element.target.parentElement

    table.childNodes.forEach((tr, index) => {
      if (tr.nodeName === 'TR' && index === 0) {
        let trH = table.childNodes[0]
        let th = document.createElement('th')
        th.style.padding = '10px'
        th.style.border = '1px solid black'
        th.style.backgroundColor = 'rgb(235,235,235)'
        th.style.borderRadius = '0px 15px 0px 0px'

        let textarea = document.createElement('textarea')
        textarea.textContent = 'add header'
        textarea.addEventListener('keyup', function () {
          this.style.height = '1px'
          this.style.height = (this.scrollHeight) + "px";
        })
        textarea.style.resize = 'none'
        textarea.style.textAlign = 'center'
        textarea.style.fontSize = '17px'
        textarea.style.border = 'none'
        textarea.style.backgroundColor = 'rgb(235,235,235)'
        textarea.style.width = '100%'
        textarea.style.height = '21px'
        textarea.style.fontFamily = "Times New Roman, Times, serif"

        th.appendChild(textarea)
        trH.appendChild(th)
      }
      if (tr.nodeName === 'TR' && index !== 0) {
        let td = document.createElement('td')
        td.style.padding = '10px'
        td.style.border = '1px solid black'

        let textarea = document.createElement('textarea')
        textarea.addEventListener('keyup', function () {
          this.style.height = '1px'
          this.style.height = (this.scrollHeight) + "px";
        })
        textarea.textContent = 'add data'
        textarea.style.resize = 'none'
        textarea.style.textAlign = 'center'
        textarea.style.fontSize = '17px'
        textarea.style.border = 'none'
        textarea.style.width = '100%'
        textarea.style.height = '21px'
        textarea.style.fontFamily = "Times New Roman, Times, serif"

        td.appendChild(textarea)
        tr.appendChild(td)
      }
    });
  }
  removeColumn(element) {
    let table = element.target.parentElement
    let row = table.rows
    for (let i = 0; i < row.length; i++) {
      let last = row[i].cells.length - 1
      row[i].deleteCell(last)
    }
  }
  removeRow(element) {
    let table = element.target.parentElement
    table.deleteRow(table.rows.length - 1)
  }
  addRow(element) {
    let table = element.target.parentElement
    let newRow = table.insertRow(table.rows.length)
    let row = table.rows
    for (let i = 0; i < row[0].cells.length; i++) {
      let cell = newRow.insertCell()
      let textarea = document.createElement('textarea')
      textarea.textContent = 'add data'
      textarea.style.resize = 'none'
      textarea.style.textAlign = 'center'
      textarea.style.fontSize = '17px'
      textarea.style.border = 'none'
      textarea.style.width = '100%'
      textarea.style.height = '21px'
      textarea.style.fontFamily = "Times New Roman, Times, serif"
      cell.style.padding = '10px'
      cell.style.border = '1px solid black'
      cell.appendChild(textarea)
    }
  }
  textAreaAdjust(element) {
    element.target.style.height = "1px";
    element.target.style.height = (element.target.scrollHeight) + "px";
  }
  adjustTextarea(element) {
    element.style.height = "1px";
    element.style.height = (element.scrollHeight) + "px";
  }

  checkStatus(element){
    if(element.target.value === 'true'){
      element.target.firstChild.style.color = 'green'
      element.target.style.color = 'green'
      element.target.childNodes[1].style.color = 'red'
    }else{
      element.target.firstChild.style.color = 'green'
      element.target.style.color = 'red'
      element.target.childNodes[1].style.color = 'red'
    }
  }
  checkStatusOnViewChecked(element){
    if(element && element.value === 'true'){
      element.firstChild.style.color = 'green'
      element.style.color = 'green'
      element.childNodes[1].style.color = 'red'
    }else if (element){
      element.firstChild.style.color = 'green'
      element.style.color = 'red'
      element.childNodes[1].style.color = 'red'
    }
  }
}
