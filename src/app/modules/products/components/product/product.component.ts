import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../../../../services/product.service'
import { environment } from 'src/environments/environment';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  formData: any = new FormData();
  products
  baseUrl = `${environment.shortUrl}`
  form: FormGroup
  file
  badRequest
  helper: number = 0
  helper2: number = 1
  limit: number = 2
  page: number = 1
  nextPage
  previousPage
  constructor(private formBuilder: FormBuilder, private productService: ProductService, private router: Router) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      Type: ['1'],
      Title: [''],
      Brand: [''],
      Code: [''],
      Images: [null],
      Description: [''],
      Info: [''],
      Published: ['false'],
    })
    this.findProducts()
  }
  onFileSelect(event) {
    this.file = (event.target as HTMLInputElement).files;
    console.log(this.file)
    this.form.patchValue({
      Images: this.file
    });
    this.form.get('Images').updateValueAndValidity()
  }
  limitChange() {
    var x = (<HTMLInputElement>document.getElementById('limitSelect')).value;
    this.limit = parseInt(x)
    this.page = 1
    this.findProducts()
  }
  findProducts = async () => {
    let response = await this.productService.findProducts(this.limit, this.page)
    this.products = response['products']['products']
    this.nextPage = response['products']['next']
    this.previousPage = response['products']['previous']
  }

  addProduct = async () => {
    try {
      let infoAndTable = document.getElementById('infoAndTable')
      let infoAndTableArray = new Array()
      if (infoAndTable.childNodes) {
        infoAndTable.childNodes.forEach(element => {
          if (element.firstChild.nodeName === "DIV") {
            let div = element.firstChild
            let textValue = (<HTMLTextAreaElement>element.childNodes[1]).value
            let selectValue = (<HTMLSelectElement>div.childNodes[1]).value
            infoAndTableArray.push({ type: selectValue, text: textValue })
          }
          if (element.firstChild.nodeName === "TABLE") {
            let table = element.firstChild
            let trHead = table.childNodes[0]
            let headArray = new Array()
            let dataArray = new Array()
            trHead.childNodes.forEach(th => {
              let headValue = (<HTMLTextAreaElement>th.firstChild).value
              headArray.push(headValue)
            });
            for (let i = 1; i < table.childNodes.length; i++) {
              let trData = table.childNodes[i]
              let dataArray2 = new Array()
              trData.childNodes.forEach(td => {
                let dataValue = (<HTMLTextAreaElement>td.firstChild).value
                dataArray2.push(dataValue)
              })
              dataArray.push(dataArray2)
            }
            console.log(headArray)
            console.log(dataArray)
            infoAndTableArray.push({ tableHeaders: headArray, tableData: dataArray })
          }
        })
        console.log(infoAndTableArray)
      }

      this.formData.append("title", this.form.get('Title').value);
      this.formData.append("type", this.form.get('Type').value);
      this.formData.append("brand", this.form.get('Brand').value);
      this.formData.append("code", this.form.get('Code').value);
      this.formData.append("published", this.form.get('Published').value);
      this.formData.append("desc", this.form.get('Description').value);
      for (let i = 0; i < this.file.length; i++) {
        this.formData.append("images[]", this.file[i])
      }
      this.formData.append("info[]", JSON.stringify(infoAndTableArray))
      await this.productService.addProduct(this.formData)
      window.location.reload();
    } catch (error) {
      console.log(error)
    }
  }
  deleteProduct = async (id) => {
    await this.productService.deleteProduct(id)
    window.location.reload()
  }
  navigate(id) {
    this.router.navigateByUrl('/products/' + id)
  }
  goToNext() {
    this.page++
    this.findProducts()
  }
  goToPrevious() {
    this.page = this.page - 1
    this.findProducts()
  }
  addInfo() {
    let infoAndTable = document.getElementById('infoAndTable')

    let informationClass = document.createElement('div')
    informationClass.className = "informationClass"
    informationClass.style.marginBottom = '10px'

    let infoSelectDiv = document.createElement('div')
    infoSelectDiv.setAttribute("class", "infoSelectDiv" + this.helper)
    infoSelectDiv.style.textAlign = "center"
    infoSelectDiv.style.position = 'relative'

    let label = document.createElement('label')
    label.setAttribute("for", "typeInfoSelect")
    label.innerText = "Type of info: "

    let select = document.createElement('select')
    select.setAttribute("id", "typeInfoSelect" + this.helper)
    select.setAttribute("name", "typeInfoSelect" + this.helper)

    let deleteButton = document.createElement('button')
    deleteButton.style.position = "absolute"
    deleteButton.style.right = "10px"
    deleteButton.style.top= "10px"
    deleteButton.textContent = "X"
    deleteButton.type = "button"
    deleteButton.style.borderRadius = '10px'
    deleteButton.style.backgroundColor = 'white'
    deleteButton.style.borderWidth = 'thin'
    deleteButton.style.display = 'block'
    deleteButton.addEventListener('click', function () {
      informationClass.remove()
    })

    let option1 = document.createElement('option')
    option1.text = "Text"
    option1.value = "text"
    let option2 = document.createElement('option')
    option2.text = "Subtitle"
    option2.value = "subtitle"

    let textarea = document.createElement('textarea')
    textarea.setAttribute("id", "infoInput" + this.helper)
    textarea.setAttribute('class', "infoInput" + this.helper)
    textarea.name = "Info"
    textarea.style.resize = "none"
    textarea.placeholder = "Information"
    textarea.style.width = "70%"
    textarea.style.height = "50px"
    textarea.style.fontSize = "14px"
    textarea.style.marginLeft = "15%"

    let br = document.createElement('br')
    let br2 = document.createElement('br')


    select.appendChild(option1)
    select.appendChild(option2)

    infoSelectDiv.appendChild(label)
    infoSelectDiv.appendChild(select)
    infoSelectDiv.appendChild(deleteButton)
    // infoSelectDiv.appendChild(deleteButton)
    infoSelectDiv.appendChild(br)
    infoSelectDiv.appendChild(br2)

    informationClass.appendChild(infoSelectDiv)
    informationClass.appendChild(textarea)

    infoAndTable.appendChild(informationClass)
    console.log(this.helper)
    this.helper++

  }
  addTable() {
    let infoAndTable = document.getElementById('infoAndTable')

    let tableDiv = document.createElement('div')
    tableDiv.style.position = 'relative'
    tableDiv.style.marginBottom = '30px'
    tableDiv.style.textAlign = 'center'
    let table = document.createElement('table')
    table.style.textAlign = 'center'
    table.style.borderCollapse = 'collapse'
    table.style.width = '95%'

    const tr = document.createElement('tr')
    tr.className = 'tableHeaders'
    const tr2 = document.createElement('tr')
    tr2.className = 'tableData'

    for (let i = 1; i < 4; i++) {
      let th = document.createElement('th')
      th.id = 'header' + i
      let textareaTH = document.createElement('textarea')
      textareaTH.style.width = '100%'
      textareaTH.style.height = '30px'
      textareaTH.style.maxHeight = '70px'
      textareaTH.style.resize = "none"
      textareaTH.style.backgroundColor = 'rgb(235,235,235)'
      textareaTH.style.outline = 'none'
      textareaTH.id = 'headerText' + i
      th.appendChild(textareaTH)
      tr.appendChild(th)

      let td = document.createElement('td')
      td.id = 'data' + i
      let textareaTD = document.createElement('textarea')
      textareaTD.style.width = '100%'
      textareaTD.style.height = '30px'
      textareaTD.style.resize = "none"
      textareaTD.style.outline = 'none'
      textareaTD.id = 'dataText' + i
      td.appendChild(textareaTD)
      tr2.appendChild(td)
    }

    let AddRowButton = document.createElement('button')
    AddRowButton.innerText = 'Add row'
    AddRowButton.type = "button"
    AddRowButton.onclick = () => {
      let tr = document.createElement('tr')
      tr.className = 'tableData'

      for (let i = 1; i < 4; i++) {
        let td = document.createElement('td')
        td.id = 'data' + i
        let textareaTD = document.createElement('textarea')
        textareaTD.style.width = '100%'
        textareaTD.style.height = '30px'
        textareaTD.style.resize = "none"
        textareaTD.style.outline = 'none'
        textareaTD.id = 'dataText' + i


        td.appendChild(textareaTD)
        tr.appendChild(td)
        table.appendChild(tr)
      }
    }

    let deleteButton = document.createElement('button')
    deleteButton.style.position = "absolute"
    deleteButton.style.right = "0px"
    deleteButton.style.top= "-4px"
    deleteButton.textContent = "X"
    deleteButton.type = "button"
    deleteButton.style.borderRadius = '10px'
    deleteButton.style.backgroundColor = 'white'
    deleteButton.style.borderWidth = 'thin'
    deleteButton.style.display = 'block'
    deleteButton.addEventListener('click', function () {
      tableDiv.remove()
    })

    table.appendChild(tr)
    table.appendChild(tr2)
    tableDiv.appendChild(table)
    tableDiv.appendChild(AddRowButton)
    tableDiv.appendChild(deleteButton)
    infoAndTable.appendChild(tableDiv)

  }
}
