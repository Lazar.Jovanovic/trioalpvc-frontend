import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './components/product/product.component';
import { ProductsRoutingModule } from './products-routing.module';
import { HeaderModule } from 'src/app/shared/components/header/header.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OneProductComponent } from './components/one-product/one-product.component';
// import {MatPaginatorModule} from '@angular/material/paginator';


@NgModule({
  declarations: [ProductComponent, OneProductComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    HeaderModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ProductsModule { }
