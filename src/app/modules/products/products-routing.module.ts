import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OneProductComponent } from './components/one-product/one-product.component';
import { ProductComponent } from './components/product/product.component';

const routes: Routes = [
  {path: '', component: ProductComponent},
  {path: ':id', component: OneProductComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
