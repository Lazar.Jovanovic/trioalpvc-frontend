import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Image } from 'src/app/model/image';
import { GalleryService } from '../../../../services/gallery.service'
import {environment} from '../../../../../environments/environment'
@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  constructor(private formBuilder: FormBuilder,private galleryService:GalleryService, private router:Router) { }
  images
  imgUrl
  form:FormGroup
  Uform:FormGroup
  baseUrl = `${environment.shortUrl}`
  confirmationText
  imageID
  limit:number = 2
  page:number = 1
  nextPage
  previousPage
  ngOnInit(): void {
    this.form = this.formBuilder.group({
      Description: [''],
      Image:[null]
    });
    this.Uform = this.formBuilder.group({
      Description: [''],
      Image:[null]
    });
    this.viewImages()
  }

  viewImages= async()=>{
    let response = await this.galleryService.viewImages(this.limit, this.page)
    this.images = response['images']['images']
    this.nextPage = response['images']['next']
    this.previousPage = response['images']['previous']
  }
  limitChange(){
    var x = (<HTMLInputElement>document.getElementById('limitSelect')).value;
    this.limit = parseInt(x)
    this.page = 1
    this.viewImages()
  }
  onFileSelect(event) {
    const file = (event.target as HTMLInputElement).files[0];
    console.log(file)
    this.form.patchValue({
      Image: file
    });
    this.form.get('Image').updateValueAndValidity()

  }
  newImage = async()=>{
    var formData: any = new FormData();
    if(this.form.get('Image').value === null){
      this.confirmationText = "No added image"
      return
    }
    formData.append("desc", this.form.get('Description').value);
    formData.append("image", this.form.get('Image').value);
    console.log(formData)
    await this.galleryService.newImage(formData)
    window.location.reload();
  }
  updateImage = async(formData, id)=>{
    var formData: any = new FormData();
    formData.append("desc", this.Uform.get('Description').value);
    formData.append("image", this.Uform.get('Image').value);
    if(this.Uform.get('Image').value === null){
      return
    }
    await this.galleryService.updateImage(formData, this.imageID)
  }
  getInfo(id,desc,image){
    // var fileElement = <HTMLInputElement>document.getElementById('imageUrl');
    // console.log(fileElement)
    // const file = fileElement['src'].split('/').pop()
    // console.log(fajl)
    // var file = fileElement.files[0];
    // const file = (image.target as HTMLInputElement).files[0];
    this.Uform.patchValue({
      Image: image,
      Description: desc
    })
    this.imageID = id
    this.Uform.get('Image').updateValueAndValidity()
    this.Uform.get('Description').updateValueAndValidity()
  }

  deleteImage = async(id)=>{
    await this.galleryService.deleteImage(id)
    window.location.reload();
  }
  goToNext(){
    this.page++
    this.viewImages()
  }
  goToPrevious(){
    this.page = this.page - 1
    this.viewImages()
  }
}
