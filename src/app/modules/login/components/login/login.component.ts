import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import {Login} from '../../../../model/login'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginInfo:Login;
  statusCode:Number;
  badRequest:String;

  constructor(private authService:AuthService, private router: Router) {
    this.loginInfo = new Login();
  }

  ngOnInit(): void {
    let token = localStorage.getItem('token')
    if(token){
      let response=  this.authService.tokenExpired(token)
      if(!response){
        this.router.navigateByUrl('/products')
      }
    }
  }

  login = async()=>{
    try {
      let response= await this.authService.authenticateUser({email:this.loginInfo.email, password: this.loginInfo.password})
      localStorage.setItem('token', response['accessToken'])
      localStorage.setItem('refreshToken', response['refreshToken'])
      this.router.navigateByUrl('/products')
    } catch (error) {
      this.badRequest = "Something went wrong"
      console.log(error)
    }
  }

}


