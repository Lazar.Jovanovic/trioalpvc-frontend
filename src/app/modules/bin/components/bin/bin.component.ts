import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../../services/product.service'
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-bin',
  templateUrl: './bin.component.html',
  styleUrls: ['./bin.component.css']
})
export class BinComponent implements OnInit {
  limit = 5
  page = 1
  products
  nextPage
  previousPage
  baseUrl = `${environment.shortUrl}`
  constructor(private productService:ProductService) { }

  ngOnInit(): void {
    this.findProducts()
  }


  limitChange() {
    var x = (<HTMLInputElement>document.getElementById('limitSelect')).value;
    this.limit = parseInt(x)
    this.page = 1
    this.findProducts()
  }
  goToNext() {
    this.page++
    this.findProducts()
  }
  goToPrevious() {
    this.page = this.page - 1
    this.findProducts()
  }
  findProducts = async () => {
    let response = await this.productService.getDeletedProducts(this.limit, this.page)
    this.products = response['products']['products']
    this.nextPage = response['products']['next']
    this.previousPage = response['products']['previous']
  }
  undelete = async(id) =>{
    await this.productService.undeleteProduct(id)
    window.location.reload()
  }
}
