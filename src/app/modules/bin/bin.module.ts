import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BinRoutingModule } from './bin-routing.module';
import { BinComponent } from './components/bin/bin.component';
import { HeaderModule } from 'src/app/shared/components/header/header.module';


@NgModule({
  declarations: [BinComponent],
  imports: [
    CommonModule,
    BinRoutingModule,
    HeaderModule
  ],
  exports: [BinComponent]
})
export class BinModule { }
