export class Product{
  published: Boolean
  type: Number
  title:String
  code: String
  brand: String
  images?:[{
    url:String
    isMail:Boolean
  }]
  desc?:String
  info?:[{
    type:Number
    text:String
    tableHeaders:[
      String
    ]
    tableData:[
      [String]
    ]
  }]
  deleted?:Boolean

}
